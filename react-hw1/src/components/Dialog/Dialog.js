import React, { Component } from 'react';
import propTypes from 'prop-types';
import './Dialog.scss';

class Dialog extends Component {
    render() {
        const { header, text, onClose } = this.props;
        const arrayActionsButtons = [
            (<button onClick={onClose} key='OK'>OK</button>),
            (<button onClick={onClose} key='Cancel'>Cancel</button>)
        ];
        let dialog = (
            <>
            <div onClick={onClose} className='backdrop'></div>
            <div className='dialog'>
                <button className='dialog-button' onClick={onClose}>x</button>
                <p>{header}</p>
                <div>{text}</div>
                <div className='dialog-button-action'>{arrayActionsButtons}</div>
            </div>
            </>
        );
        console.log(dialog);

        return (
            <div>
            { dialog }
            </div>
        )
    }
};

Dialog.propTypes = {
    header: propTypes.string.isRequired,
    text: propTypes.string.isRequired,
    onClose: propTypes.func.isRequired
};

Dialog.defaulsProps = {
    header: 'Первый раз на сайте?',
    text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi placeat sequi facere quia veniam expedita tempore.'
}


export default Dialog;
