import React, { Component } from 'react';
import propTypes from 'prop-types';
import './Button.scss';

class Button extends Component {
    render() {
        const { text, color, onClick } = this.props;
        return(
            <div>
                <button className='button-modalOpen' style={{backgroundColor: color}} onClick={onClick}>{text}</button>
            </div>
        )
    }
}

Button.propTypes = {
    text: propTypes.string.isRequired,
    color: propTypes.string.isRequired,
    onClick: propTypes.func.isRequired
};

Button.defaultProps = {
    text: 'Open first modal',
    color: '#5F9EA0'
}

export default Button;
