import React, { Component } from 'react';
import './App.scss'
import Dialog from './components/Dialog/Dialog';
import Button from './components/Button/Button';

class App extends Component {
  state = {
    isOpen: null
  };

  openFirstModal = () => {
    this.setState({ isOpen: "first"});
  };

  openSecondModal = () => {
    this.setState({ isOpen: "second"});
  };

  onClose = () => {
    this.setState({ isOpen: null});
  };

  render() {
    
    const modals = {
      first: (
        <Dialog 
              header='Первый раз на сайте?'   
              text='Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi placeat sequi facere quia veniam expedita tempore.'
              onClose={this.onClose} >
              {/* colorTheme={[]} */}
        </Dialog>
      ),
      second: (
        <Dialog 
              header='First time on site?'   
              text='Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi placeat sequi facere quia veniam expedita tempore.'
              onClose={this.onClose} >
              {/* colorTheme={['dialog']} */}
        </Dialog>
      )
    };

    return (
      <div className="App">
        <div className='button-block'>
          <Button text={'Open first modal'} color='#5F9EA0' onClick={this.openFirstModal} />
          <Button text={'Open second modal'} color='#A9A9A9' onClick={this.openSecondModal} /> 
          {modals[this.state.isOpen]}
        </div>
      </div>
    );
  }
}

export default App;
