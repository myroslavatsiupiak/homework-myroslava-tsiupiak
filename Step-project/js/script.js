// -------------------------theHam header--------------------

$(`.nav-menu a`).mouseover(function () {
    $(`.nav-menu a`).remove(`menu-active`);
    $(this).addClass(`menu-active`);
});
$(`.nav-menu a`).mouseout(function () {
    $(`.nav-menu a`).removeClass(`menu-active`);
});

// ----------------------What People Say About TheHam--------------------------

$(".slider-pic").on("click", function(event) {
        $(this).addClass('slider-active').siblings().removeClass('slider-active');
        const index = $(this).index();
        changeInfo(index);
})
$(".slider-arrow").on("click", function() {
    if ($(this).hasClass("arrow-left")) {
        const activeIndex = $(".slider-active").index()
        const previousActiveIndex = activeIndex == 0 ? 3 : activeIndex-1;
        console.log(previousActiveIndex);
        $(`.slider-pic:eq(${previousActiveIndex})`).addClass("slider-active").siblings().removeClass("slider-active")
        changeInfo(previousActiveIndex);
    }
    if ($(this).hasClass("arrow-right")) {
        const activeIndex = $(".slider-active").index()
        const nextActiveIndex = activeIndex == 3 ? 0 : activeIndex+1;
        console.log(nextActiveIndex);
        $(`.slider-pic:eq(${nextActiveIndex})`).addClass("slider-active").siblings().removeClass("slider-active")
        changeInfo(nextActiveIndex);
    }
})

function changeInfo(index) {
    $('.slider-people-index').eq(index)
        .fadeIn(1000)
        .siblings()
        .hide();
}


// -------------------------------Our Services---------------------------

const tabs = document.querySelectorAll(".tabs__caption");

$(".tabs__caption li").click(function(e) {
$(this).addClass("active-tabs").siblings().removeClass("active-tabs");
const index = $(this).index();
$(".tabs__content").removeClass("active-tabs").eq(index).addClass("active-tabs")
})


// --------------------------------Our Amazing Work-------------------------------------

$(document).ready(function () {
    $(".portfolio-item .portfolio-item-and-hover").hide().slice(0, 12).show();

    $(`.portfolio-menu-ul li`).click(function () {
        $(this).siblings().removeClass(`active`);
        $(this).addClass(`active`);
        const selector = $(this).attr(`data-filter`);
        $(".portfolio-item .portfolio-item-and-hover").fadeOut(400);
        $(`.portfolio-item ${selector}.portfolio-item-and-hover`).fadeIn(400);
        if ($(`.portfolio-item ${selector}.portfolio-item-and-hover`).length <= 12) {
            $(".load-more-button").fadeOut(400)
        }
        else {
            $(".load-more-button").fadeIn(400)
        }
    });

    $(".load-more-button").click(function () {
        const category = $(`.portfolio-menu-ul li.active`).data("filter");
        $(`.portfolio-item ${category}.portfolio-item-and-hover:hidden`).slice(0, 12).fadeIn(1000);
        if (!$(`.portfolio-item ${category}.portfolio-item-and-hover:hidden`).length) {
            $(".load-more-button").remove();
        }
    })
})


// $(".item").mouseover(function() {
//     $(this).
// })

    // $(`.portfolio-item`).isotope({
    //     itemSelector: `.item`,
    //     layoutMode: `fitRows`
    // });

    // $(`.portfolio-menu-ul li`).click(function() {
    //     $(`.portfolio-menu-ul li`).removeClass(`active`);
    //     $(this).addClass(`active`);
    //     const selector = $(this).attr(`data-filter`);
    //     console.log(selector)
    //     $(`.portfolio-item`).isotope({
    //         filter: selector
    //     });
    //     return false;
    // });










