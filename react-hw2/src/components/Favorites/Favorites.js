import React, { Component } from 'react';
import './Favorites.scss';

class Favorites extends Component {
    render() {
        const { onClick, isFav } = this.props;
        const emptyClasses = 'fav fav-star';
        const filledClasses = 'fav fav-star filled';
        return (
            <div className={isFav ? filledClasses : emptyClasses}  onClick={onClick}></div>
        )
    }
};


export default Favorites;