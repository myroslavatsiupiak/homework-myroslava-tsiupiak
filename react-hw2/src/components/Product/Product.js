import React, { Component } from 'react';
import './Product.scss';
import Button from '../Button/Button';
import Favorites from '../Favorites/Favorites';

class Product extends Component {
    render() {
        const { product, header, openModal, isFav, onSaveToFav } = this.props;
        return (
            <div className='products'>
                <div className='products-img'>
                    <img src={product.image} className='products-img-item' alt={product.article}></img>
                </div>
                <div className='products-description'>
                    <div className='products-description-name'>{product.name}</div>
                    <div className='products-description-price'>{product.price}</div>
                    <div className='products-description-color'>
                        <div className='products-description-color-container'>
                            <div className='products-description-color-container-square' style={{ backgroundColor: product.colorId }}></div>
                            {product.color}
                        </div>
                        <div className='action-block'>
                            <Favorites isFav={isFav} onClick={() => onSaveToFav(product)} />
                            <Button text={header} onClick={() => openModal(product)} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
};

export default Product;