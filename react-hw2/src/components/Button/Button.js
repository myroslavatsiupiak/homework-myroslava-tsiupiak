import React, { Component } from 'react';
import propTypes from 'prop-types';
import './Button.scss';

class Button extends Component {
    render() {
        const { text, onClick } = this.props;
        return (
            <div>
                <button className="btn" onClick={onClick}>{text}</button>
            </div>
        )
    }
}

Button.propTypes = {
    text: propTypes.string,
    onClick: propTypes.func.isRequired
};

Button.defaultProps = {
    text: 'Add to cart'
}

export default Button;