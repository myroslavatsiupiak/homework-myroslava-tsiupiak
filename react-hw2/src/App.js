import React, { Component } from 'react';
import axios from 'axios';
import Loading from './components/Loading/Loading';
import Products from './containers/Products';
import Dialog from './components/Dialog/Dialog';

class App extends Component {

  state = {
    isOpen: false,
    products: [],
    loading: true,
    currentProduct: null,
    cart: JSON.parse(localStorage.getItem('cart')) || [],
    favorites: JSON.parse(localStorage.getItem('favorites')) || []
  };

  componentDidMount() {

    axios('/products.json')
      .then(result => {
        setTimeout(() => {
          this.setState({
            loading: false,
            products: result.data
          });
        }, 2000);
        // console.log(result.data)
      });
  };
  openModal = (product) => {
    this.setState({
      isOpen: true,
      currentProduct: product
    })
  };
  onClose = () => {
    this.setState({ isOpen: false })
  };
  onSubmit = (product) => {
    this.setState((prevState) => {
      const cart = [...prevState.cart, product];
      localStorage.setItem("cart", JSON.stringify(cart));
      return ({
        isOpen: false,
        cart: cart
      })
    });
  };
  onSaveToFav = (newProduct) => {
    this.setState((prevState) => {
      let newFavorites = [...prevState.favorites];
      const indexFav = newFavorites.indexOf(newProduct);
      
      
      if (indexFav !== -1) {
        newFavorites.splice(indexFav, 1);
      } else {
        newFavorites.push(newProduct);
      }
      localStorage.setItem('favorites', JSON.stringify(newFavorites));


      return ({
        favorites: newFavorites,
        currentProduct: newProduct
      })

    })
  };


  render() {
    const { loading, isOpen, currentProduct } = this.state;
    // console.log('currentProduct', currentProduct)
    // console.log('current state CART:', this.state.cart);
    // console.log('current state of FAVORITES', this.state.favorites);

    const dialog = (
      <Dialog
        header='Аdd product to cart?'
        onClose={this.onClose}
        onSubmit={() => this.onSubmit(currentProduct)}
        currentProduct={currentProduct}
        >
      </Dialog>
    )

    if (loading) {
      return <Loading />
    }
    return (
      <div className="App">
        <Products products={this.state.products} openModal={this.openModal} onSaveToFav={this.onSaveToFav} favorites={this.state.favorites} />
        {isOpen && dialog}
      </div>
    );
  }

}

export default App;
