import React, { Component } from 'react';
import Product from '../components/Product/Product';
import './Products.scss';

class Products extends Component {
    render() {
        const { products, openModal, favorites, onSaveToFav } = this.props;
        const favoritesArticle = favorites.map(fav => fav.article);
        const productCards = products.map(product => {
            const isFav = favoritesArticle.includes(product.article)
            return <Product isFav={isFav} key={product.article} product={product} openModal={openModal} onSaveToFav={onSaveToFav} />
        })
        return(
            <div className='products'>
                {productCards}
            </div>
        ) 
    }
};

export default Products;