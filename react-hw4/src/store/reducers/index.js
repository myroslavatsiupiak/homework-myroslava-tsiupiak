import { combineReducers } from 'redux';
import products from './productsReducer';
import cart from './cartReducer';
import favorites from './favoritesReducer';
import loading from './loadingReducer';
import currentProduct from './currentProductReducer';
import openingModal from "./openingModalReducer";
import isToDelete from './deleteCartReducer';

const rootReducer = combineReducers({
    products,
    favorites,
    cart,
    loading,
    currentProduct,
    openingModal,
    isToDelete
})

export default rootReducer;