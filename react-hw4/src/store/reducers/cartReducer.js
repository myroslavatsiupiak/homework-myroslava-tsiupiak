import Actions from "../constants/constants";

const cart = (state = JSON.parse(localStorage.getItem('cart')) || [], action) => {
    switch (action.type) {
        case Actions.SET_CART:
            return action.payload;
        default:
            return state
    }
};

export default cart;