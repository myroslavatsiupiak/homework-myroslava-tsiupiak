import Actions from '../constants/constants';

export const setLoading = (loadingBool) => {
    return {
        type: Actions.SET_LOADING,
        payload: loadingBool
    }
};

export const setProducts = (products) => {
    return {
        type: Actions.FETCH_PRODUCTS,
        payload: products
    }
};

export const setCurrentProduct = (currentProduct) => {
    return {
        type: Actions.SET_CURRENT_PRODUCT,
        payload: currentProduct
    }
}

export const setOpenModal = (isOpen) => {
    return {
        type: Actions.SET_OPEN_MODAL,
        payload: isOpen
    }
};

export const setFavorites = (favs) => {
    return {
        type: Actions.SET_FAVORITES,
        payload: favs
    }
};

export const setCart = (cartCard) => {
    return {
    type: Actions.SET_CART,
        payload: cartCard
    }
};

export const deleteCartCart = (prod) => {
    return {
        type: Actions.DELETE_CART_CARD,
        payload: prod
    }
}