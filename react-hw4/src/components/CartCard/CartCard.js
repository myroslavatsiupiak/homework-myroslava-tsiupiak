import React from 'react';
import Favorites from '../Favorites/Favorites';
import './CartCard.scss';
import propTypes from 'prop-types';
import Dialog from '../Dialog/Dialog';
import { connect } from 'react-redux';
import { deleteCartCart, setCart, setCurrentProduct } from '../../store/actions/actions';

const CartCard = (props) => {
    const { isToDelete, setOpenModalDeleteCart, saveToCart, saveCurProd,
        currentProduct, onSaveToFav, isFav, cartProduct, cart } = props;

    const openModalDelete = (product) => {
        setOpenModalDeleteCart(true);
        saveCurProd(product);
    };
    const onClose = () => {
        setOpenModalDeleteCart(false);
    };
    const onDelete = (product) => {
        let newCart = [...cart];
        const indexCart = newCart.indexOf(product);

        if (indexCart !== -1) {
            newCart.splice(indexCart, 1)
        }
        localStorage.setItem('cart', JSON.stringify(newCart));
        console.log(newCart);

        saveCurProd(product);
        setOpenModalDeleteCart(false);
        saveToCart(newCart);
    };
    const dialogDetele = (
        <Dialog
            header='Do you really want to delete product from cart?'
            onClose={onClose}
            onSubmit={() => onDelete(currentProduct)}
            currentProduct={currentProduct}
        >
        </Dialog>
    );

    if (isToDelete) {
        return dialogDetele
    }
    return (
        <div className='cart-card'>
            <div className='cart-action'>
                <Favorites className='cart-action-fav' isFav={isFav} onClick={() => onSaveToFav(cartProduct)} />
                <div className='cart-action-delete' onClick={() => openModalDelete(cartProduct)}>
                    <img src="https://img.icons8.com/small/16/000000/delete-sign.png" alt={cartProduct.article} />
                </div>
            </div>
            <div className='products'>
                <div className='products-img'>
                    <img src={cartProduct.image} className='products-img-item' alt={cartProduct.article}></img>
                </div>
                <div className='products-description'>
                    <div className='products-description-name'>{cartProduct.name}</div>
                    <div className='products-description-price'>{cartProduct.price}</div>
                    <div className='products-description-color'>
                        <div className='products-description-color-container'>
                            <div className='products-description-color-container-square' style={{ backgroundColor: cartProduct.colorId }}></div>
                            {cartProduct.color}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

CartCard.propTypes = {
    onSaveToFav: propTypes.func,
    isFav: propTypes.bool,
    cartProduct: propTypes.object,
    onDelete: propTypes.func
}

const mapStateToProps = store => {
    return {
        isToDelete: store.isToDelete,
        cart: store.cart,
        currentProduct: store.currentProduct
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setOpenModalDeleteCart: (card) => dispatch(deleteCartCart(card)),
        saveToCart: (cartItem) => dispatch(setCart(cartItem)),
        saveCurProd: (product) => dispatch(setCurrentProduct(product))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartCard);