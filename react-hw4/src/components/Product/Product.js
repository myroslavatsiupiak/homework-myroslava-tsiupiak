import React from 'react';
import './Product.scss';
import Button from '../Button/Button';
import Favorites from '../Favorites/Favorites';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { setOpenModal, setCurrentProduct } from '../../store/actions/actions'

const Product = (props) => {
    const { openingModal, saveCurProd, product, header, isFav, isInCart, onSaveToFav } = props;

    const openModal = (product) => {
        openingModal(true);
        saveCurProd(product)
    };

    return (
        <div className='products'>
            <div className='products-img'>
                <img src={product.image} className='products-img-item' alt={product.article}></img>
            </div>
            <div className='products-description'>
                <div className='products-description-name'>{product.name}</div>
                <div className='products-description-price'>{product.price}</div>
                <div className='products-description-color'>
                    <div className='products-description-color-container'>
                        <div className='products-description-color-container-square' style={{ backgroundColor: product.colorId }}></div>
                        {product.color}
                    </div>
                    <div className='action-block'>
                        <Favorites isFav={isFav} onClick={() => onSaveToFav(product)} />
                        <Button isInCart={isInCart} text={header} onClick={() => openModal(product)} />
                    </div>
                </div>
            </div>
        </div>
    )
};

Product.propTypes = {
    onSaveToFav: propTypes.func,
    product: propTypes.object,
    openModal: propTypes.func,
    isFav: propTypes.bool,
    isInCart: propTypes.bool,
}

const mapStateToProps = store => {
    return {
        opModal: store.openingModal
    }
};

const mapDispatchToProps = dispatch => {
    return {
        openingModal: (isOpen) => dispatch(setOpenModal(isOpen)),
        saveCurProd: (product) => dispatch(setCurrentProduct(product)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);