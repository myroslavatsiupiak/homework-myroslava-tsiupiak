import React, { useEffect } from 'react';
import axios from 'axios';
import { Switch, Route } from 'react-router-dom';
import Loading from './components/Loading/Loading';
import Products from './containers/Products/Products';
import Dialog from './components/Dialog/Dialog';
import Header from './containers/Header/Header';
import Cart from './containers/Cart/Cart';
import Favorites from './containers/Favorites/Favorites';
import { connect } from 'react-redux';
import { setLoading, setProducts, setCurrentProduct, setFavorites, setCart, setOpenModal } from './store/actions/actions';

function App(props) {

  const { loading, setLoading, saveProducts,
        currentProduct, saveCurProd, openingModal, setOpeningModal,
        saveFavs, favorites, cart, saveToCart } = props;

  useEffect(() => {
    axios('/products.json')
      .then(result => {
        setTimeout(() => {
          setLoading(false);
          saveProducts(result.data)
        }, 2000);
        // console.log(result.data)
      });
  }, [setLoading, saveProducts]);

  const onClose = () => {
    setOpeningModal(false);
  };

  const onSubmit = (product) => {
    const newCart = [...cart, product];
    localStorage.setItem("cart", JSON.stringify(newCart));
    saveToCart(newCart);
    setOpeningModal(false);
  };
  
  const onSaveToFav = (newProduct) => {
    let newFavorites = [...favorites];
    const indexFav = newFavorites.indexOf(newProduct);

    if (indexFav !== -1) {
      newFavorites.splice(indexFav, 1);
    } else {
      newFavorites.push(newProduct);
    }
    localStorage.setItem('favorites', JSON.stringify(newFavorites));
    saveFavs(newFavorites);
    saveCurProd(newProduct)
  };

  const dialog = (
    <Dialog
      header='Аdd product to cart?'
      onClose={onClose}
      onSubmit={() => onSubmit(currentProduct)}
      currentProduct={currentProduct}
    >
    </Dialog>
  )


  if (loading) {
    return <Loading />
  }
  if (openingModal) {
    return dialog
  }


  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path='/main' render={() => <Products onSaveToFav={onSaveToFav} />} />
        <Route exact path='/favorites' render={() => <Favorites onSaveToFav={onSaveToFav} />} />
        <Route exact path='/cart' render={() => <Cart onSaveToFav={onSaveToFav} />} />
      </Switch>
    </div>
  );

}

const mapStateToProps = (store) => {
  return {
    loading: store.loading,
    products: store.products,
    currentProduct: store.currentProduct,
    openingModal: store.openingModal,
    favorites: store.favorites,
    cart: store.cart
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLoading: (loadingBool) => dispatch(setLoading(loadingBool)),
    saveProducts: (products) => dispatch(setProducts(products)),
    saveCurProd: (product) => dispatch(setCurrentProduct(product)),
    saveFavs: (fav) => dispatch(setFavorites(fav)),
    saveToCart: (cartItem) => dispatch(setCart(cartItem)),
    setOpeningModal: (isOpen) => dispatch(setOpenModal(isOpen)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);