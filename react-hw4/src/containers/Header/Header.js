import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.scss';

const Header = () => {

    return (
        <div className='header'>
            <div>
                <NavLink exact to='/main'>
                    <img src="https://img.icons8.com/ios/35/000000/home.png" alt='main-icon' />
                </NavLink>
            </div>
            <div>
                <NavLink exact to='/favorites'>
                    <img src="https://img.icons8.com/dotty/40/000000/window-favorite.png" alt='favorites-icon' />
                </NavLink>
            </div>
            <div>
                <NavLink exact to='/cart'>
                    <img src="https://img.icons8.com/dotty/40/000000/favorite-cart.png" alt='cart-icon' />
                </NavLink>
            </div>
        </div>
    );
}

export default Header;