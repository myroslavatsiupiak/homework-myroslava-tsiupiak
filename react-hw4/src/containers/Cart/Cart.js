import React from 'react';
import CartCard from '../../components/CartCard/CartCard';
import propTypes from 'prop-types';
import { connect } from 'react-redux';

const Cart = (props) => {
    const { cart, favorites, onSaveToFav } = props;
    const favoritesArticle = favorites.map(fav => fav.article);
    const cartProducts = cart.map(cartProduct => {
        const isFav = favoritesArticle.includes(cartProduct.article);
        return <CartCard key={cartProduct.article} cartProduct={cartProduct} isFav={isFav} onSaveToFav={onSaveToFav} />
    })
    return (
        <div className='products'>
            {cart.length ? cartProducts : <div className='no-prod'>No products in Cart</div>}
        </div>
    )
};

Cart.propTypes = {
    cart: propTypes.array,
    onSaveToFav: propTypes.func,
    favorites: propTypes.array
}

const mapStateToProps = (store) => {
    return {
        favorites: store.favorites,
        cart: store.cart
    }
}

export default connect(mapStateToProps, null)(Cart);
