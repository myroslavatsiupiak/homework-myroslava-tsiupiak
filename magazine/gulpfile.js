const {src, dest, watch, parallel, series} = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const plumber = require("gulp-plumber");
const sourcemaps = require("gulp-sourcemaps");
const minifyCSS = require("gulp-clean-css");
const minifyJS = require("gulp-uglify");
const rigger = require("gulp-rigger")
const imageminn = require("gulp-imagemin")
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const clean = require("gulp-clean");
const browserSync = require("browser-sync").create();

const path = {
    src: {
        html:   'src/template/index.html',
        js:     'src/js/**/*.js',
        styles: 'src/scss/**/*.scss',
        img:    'src/img/**/*.*',
    },
    build: {
        html:   'build',
        js:     'build/js',
        styles: 'build/css',
        img:    'build/img',
    },
    watch: {
        html:   'src/template/**/*.html',
        js:     'src/js/**/*.js',
        styles: 'src/scss/**/*.scss',
        img:    'src/img/**/*.*',
    },
    clean: 'build/*'
}

const fontsFiles = [
    './src/fonts/**.eot',
    './src/fonts/**.ttf',
    './src/fonts/**.woff',
    './src/fonts/**.otf'
];

function html() {
    return src(path.src.html)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(dest(path.build.html))
        .pipe(browserSync.stream());
}

function js() {
    return src(path.src.js)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(dest(path.build.js))
        .pipe(minifyJS())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(dest(path.build.js))
        .pipe(browserSync.stream());
}

function css() {
    return src(path.src.styles)
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer({
            cascade: false,
        }))
        .pipe(dest(path.build.styles))
        .pipe(minifyCSS({
            level: 2
        }))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(dest(path.build.styles))
        .pipe(browserSync.stream());
}

function img() {
    return src(path.src.img)
        // .pipe(imageminn())
        .pipe(dest(path.build.img))
        .pipe(browserSync.stream());
}

function fonts() {
    return src(fontsFiles)
        .pipe(dest('./build/fonts'))
}

function del() {
    return src(path.clean, {read: false})
        .pipe(clean())
}

function watcher() {
    browserSync.init({
        server: {
            baseDir: "./build"
        },
        browser: ["google chrome"],
    });
    watch(path.watch.html, html);
    watch(path.watch.js, js);
    watch(path.watch.styles, css);
    watch(path.watch.img, img);
}

const build = series(del, parallel(html, js, css, img, fonts));
const dev = series(build, watcher);

exports.html = html;
exports.js = js;
exports.css = css;
exports.img = img;
exports.fonts = fonts;
exports.del = del;
exports.watcher = watcher;
exports.build = build;
exports.dev = dev;
exports.default = dev;