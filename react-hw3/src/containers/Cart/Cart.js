import React from 'react';
import CartCard from '../../components/CartCard/CartCard';
import propTypes from 'prop-types';

const Cart = (props) => {
    const { cart, onSaveToFav, favorites, setCart, isOpen, setIsOpen } = props;
    const favoritesArticle = favorites.map(fav => fav.article);
    const cartProducts = cart.map(cartProduct => {
        const isFav = favoritesArticle.includes(cartProduct.article);
        return <CartCard key={cartProduct.article} cartProduct={cartProduct}
            onSaveToFav={onSaveToFav} isFav={isFav} cart={cart}
            setCart={setCart} isOpen={isOpen} setIsOpen={setIsOpen} />
    })
    return (
        <div className='products'>
            {cart.length ? cartProducts : <div className='no-prod'>No products in Cart</div>}
        </div>
    )
};

Cart.propTypes = {
    cart: propTypes.array,
    onSaveToFav: propTypes.func,
    favorites: propTypes.array
}

export default Cart;
