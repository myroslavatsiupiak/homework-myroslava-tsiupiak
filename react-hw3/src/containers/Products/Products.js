import React from 'react';
import Product from '../../components/Product/Product';
import './Products.scss';
import propTypes from 'prop-types';

const Products = (props) => {

    const { products, openModal, favorites, onSaveToFav, cart } = props;
    const favoritesArticle = favorites.map(fav => fav.article);
    const cartArticle = cart.map(cartItem => cartItem.article)
    const productCards = products.map(product => {
        const isFav = favoritesArticle.includes(product.article);
        const isInCart = cartArticle.includes(product.article)
        return <Product isFav={isFav} isInCart={isInCart} key={product.article} product={product} openModal={openModal} onSaveToFav={onSaveToFav} />
    })
    return (
        <div className='products'>
            {productCards}
        </div>
    )
};

Product.propTypes = {
    products: propTypes.array,
    openModal: propTypes.func,
    favorites: propTypes.array
}

export default Products;