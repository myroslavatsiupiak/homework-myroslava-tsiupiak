import React from 'react';
import FavoriteCard from '../../components/FavoriteCard/FavoriteCard';
import './Favorites.scss';
import propTypes from 'prop-types';

const Favorites = (props) => {
    const { onSaveToFav, favorites } = props;
    const favoritesArticle = favorites.map(fav => fav.article);
    const favoriteProducts = favorites.map(favProduct => {
        const isFav = favoritesArticle.includes(favProduct.article);
        return <FavoriteCard key={favProduct.article} isFav={isFav} favProduct={favProduct} onSaveToFav={onSaveToFav} />
    })
    return (
        <div className='products'>
            {favorites.length ? favoriteProducts : <div className='no-prod'>No Products in favorites</div>}
        </div>
    )
};

Favorites.propTypes = {
    onSaveToFav: propTypes.func,
    favorites: propTypes.array,
}

export default Favorites;