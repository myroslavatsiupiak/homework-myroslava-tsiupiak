import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Switch, Route } from 'react-router-dom';
import Loading from './components/Loading/Loading';
import Products from './containers/Products/Products';
import Dialog from './components/Dialog/Dialog';
import Header from './containers/Header/Header';
import Cart from './containers/Cart/Cart';
import Favorites from './containers/Favorites/Favorites';

function App() {

  const [isOpen, setIsOpen] = useState(false);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentProduct, setCurrentProduct] = useState(null);
  const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
  const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);

  useEffect(() => {
    axios('/products.json')
      .then(result => {
        setTimeout(() => {
          setLoading(false);
          setProducts(result.data)
        }, 2000);
        // console.log(result.data)
      });
  }, []);

  const openModal = (product) => {
    setIsOpen(true);
    setCurrentProduct(product)
  };

  const onClose = () => {
    setIsOpen(false);
  };

  const onSubmit = (product) => {
    const newCart = [...cart, product];
    localStorage.setItem("cart", JSON.stringify(newCart));
    setCart(newCart);
    setIsOpen(false);
  };
  
  const onSaveToFav = (newProduct) => {
    let newFavorites = [...favorites];
    const indexFav = newFavorites.indexOf(newProduct);

    if (indexFav !== -1) {
      newFavorites.splice(indexFav, 1);
    } else {
      newFavorites.push(newProduct);
    }
    localStorage.setItem('favorites', JSON.stringify(newFavorites));
    setFavorites(newFavorites);
    setCurrentProduct(newProduct)
  };

  const dialog = (
    <Dialog
      header='Аdd product to cart?'
      onClose={onClose}
      onSubmit={() => onSubmit(currentProduct)}
      currentProduct={currentProduct}
    >
    </Dialog>
  )


  if (loading) {
    return <Loading />
  }
  if (isOpen) {
    return dialog
  }


  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path='/main' render={() => <Products products={products} openModal={openModal} onSaveToFav={onSaveToFav} favorites={favorites} cart={cart} />} />
        <Route exact path='/favorites' render={() => <Favorites favorites={favorites} onSaveToFav={onSaveToFav} />} />
        <Route exact path='/cart' render={() => <Cart cart={cart} setCart={setCart} onSaveToFav={onSaveToFav} favorites={favorites} isOpen={isOpen} setIsOpen={setIsOpen} />} />
      </Switch>
    </div>
  );

}

export default App;


