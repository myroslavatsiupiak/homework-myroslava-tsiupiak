import React from 'react';
import './Product.scss';
import Button from '../Button/Button';
import Favorites from '../Favorites/Favorites';
import propTypes from 'prop-types';

const Product = (props) => {
    const { product, header, openModal, isFav, isInCart, onSaveToFav } = props;
    return (
        <div className='products'>
            <div className='products-img'>
                <img src={product.image} className='products-img-item' alt={product.article}></img>
            </div>
            <div className='products-description'>
                <div className='products-description-name'>{product.name}</div>
                <div className='products-description-price'>{product.price}</div>
                <div className='products-description-color'>
                    <div className='products-description-color-container'>
                        <div className='products-description-color-container-square' style={{ backgroundColor: product.colorId }}></div>
                        {product.color}
                    </div>
                    <div className='action-block'>
                        <Favorites isFav={isFav} onClick={() => onSaveToFav(product)} />
                        <Button isInCart={isInCart} text={header} onClick={() => openModal(product)} />
                    </div>
                </div>
            </div>
        </div>
    )
};

Product.propTypes = {
    onSaveToFav: propTypes.func,
    product: propTypes.object,
    openModal: propTypes.func,
    isFav: propTypes.bool,
    isInCart: propTypes.bool,
}

export default Product;