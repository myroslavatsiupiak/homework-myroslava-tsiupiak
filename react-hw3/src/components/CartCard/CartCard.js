import React, { useState } from 'react';
import Favorites from '../Favorites/Favorites';
import './CartCard.scss';
import propTypes from 'prop-types';
import Dialog from '../Dialog/Dialog';

const CartCard = (props) => {
    const { onSaveToFav, isFav, cartProduct, cart, setCart } = props;

    const [isOpenDetele, setIsOpenDelete] = useState(false);
    const [currentProduct, setCurrentProduct] = useState(null);

    const openModalDelete = (product) => {
        setIsOpenDelete(true);
        setCurrentProduct(product)
    };
    const onClose = () => {
        setIsOpenDelete(false);
      };
    const onDelete = (product) => {
        let newCart = [...cart];
        const indexCart = newCart.indexOf(product);
    
        if (indexCart !== -1) {
          newCart.splice(indexCart, 1)
        }
        localStorage.setItem('cart', JSON.stringify(newCart));
        console.log(newCart);
        setCart(newCart);
        setCurrentProduct(product);
        setIsOpenDelete(false)
      };
    const dialogDetele = (
        <Dialog
            header='Do you really want to delete product from cart?'
            onClose={onClose}
            onSubmit={() => onDelete(currentProduct)}
            currentProduct={currentProduct}
        >
        </Dialog>
    );

    if (isOpenDetele) {
        return dialogDetele
    }
    return (
        <div className='cart-card'>
            <div className='cart-action'>
                <Favorites className='cart-action-fav' isFav={isFav} onClick={() => onSaveToFav(cartProduct)} />
                <div className='cart-action-delete' onClick={() => openModalDelete(cartProduct)}>
                    <img src="https://img.icons8.com/small/16/000000/delete-sign.png" alt={cartProduct.article} />
                </div>
            </div>
            <div className='products'>
                <div className='products-img'>
                    <img src={cartProduct.image} className='products-img-item' alt={cartProduct.article}></img>
                </div>
                <div className='products-description'>
                    <div className='products-description-name'>{cartProduct.name}</div>
                    <div className='products-description-price'>{cartProduct.price}</div>
                    <div className='products-description-color'>
                        <div className='products-description-color-container'>
                            <div className='products-description-color-container-square' style={{ backgroundColor: cartProduct.colorId }}></div>
                            {cartProduct.color}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

CartCard.propTypes = {
    onSaveToFav: propTypes.func,
    isFav: propTypes.bool,
    cartProduct: propTypes.object,
    cart: propTypes.array,
    setCart: propTypes.func
}

export default CartCard;
