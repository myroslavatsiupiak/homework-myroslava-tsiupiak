import React, { useEffect } from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';

import Product from '../../components/Product/Product';
import { getTotal } from '../../store/actions/actions';
import './Products.scss';

const Products = (props) => {

    const { products, favorites, onSaveToFav, cart, getTotal } = props;

    useEffect(() => {
        getTotal()
    }, [getTotal])

    const favoritesArticle = favorites.map(fav => fav.article);
    const cartArticle = cart.map(cartItem => cartItem.article);
    const productCards = products.map(product => {
        const isFav = favoritesArticle.includes(product.article);
        const isInCart = cartArticle.includes(product.article)
        return <Product isFav={isFav} isInCart={isInCart} key={product.article} product={product} onSaveToFav={onSaveToFav} />
    })
    return (
        <div className='products'>
            {productCards}
        </div>
    )
};

Products.propTypes = {
    products: propTypes.array,
    onSaveToFav: propTypes.func,
    favorites: propTypes.array,
    cart: propTypes.array
}

Products.defaultProps = {
    products: []
}

const mapStateToProps = (store) => {
    return {
        products: store.products,
        cart: store.cart.cartArray,
        favorites: store.favorites,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTotal: () => dispatch(getTotal())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);