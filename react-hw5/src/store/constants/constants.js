const Actions = {
    FETCH_PRODUCTS: 'FETCH_PRODUCTS',
    SET_CART: 'SET_CART',
    SET_FAVORITES: 'SET_FAVORITES',
    SET_LOADING: 'SET_LOADING',
    SET_CURRENT_PRODUCT: 'SET_CURRENT_PRODUCT',
    SET_OPEN_MODAL: 'SET_OPEN_MODAL',
    DELETE_CART_CARD: 'DELETE_CART_CARD',
    SET_PURCHASE_DATA: 'SET_PURCHASE_DATA',
    SET_AMOUNT: 'SET_AMOUNT',
    DECREASE_AMOUNT: 'DECREASE_AMOUNT',
    INCREASE_AMOUNT: 'INCREASE_AMOUNT',
    CLEAR_CART: 'CLEAR_CART',
    GET_TOTAL: 'GET_TOTAL'
}

export default Actions;