const token = '7fd08d813e37';
const login = 'stepproject@gmail.com';
const password = 'fem-5';

const url = 'http://cards.danit.com.ua/cards';

class Modal {
    constructor(classList, ...elements) {
        this._classList = classList.join(' '),
            this._elements = elements,
            this._modal = null
    }
    render(container) {
        this._modal = document.createElement('div');
        this._modal.className = 'modal-content';
        this._modal.innerHTML = `
        <span class='close'>x</span>
 `;
        this._elements.forEach(element => {
            console.log(element);
            this._modal.appendChild(element.render());
        })
        // this._modal.className = this._classList.join(' ');
        // this._modal.id = this._id;
        const openAutoForm = document.getElementById('btn');
        openAutoForm.addEventListener('click', () => {
            modal.show();
            document.body.append(this._modal);
        });

        this._modal.addEventListener('click', (event) => {
            if (event.target.classList.contains('close')) {
                this.hide();
            };
        });
    }

    show() {
        this._modal.classList.add('active');
    }
    hide() {
        this._modal.classList.remove('active');
    }
};


class Form {
    constructor(header, className, functionSubmit, ...args) {
        this._header = header,
            this._className = className,
            this.functionSubmit = functionSubmit,
            this._args = args,
            this.form = null
            this.select = null
    }
    render() {
        console.log(this._args)
        this.form = document.createElement('form');
        this.form.header = this._header;
        this.form.innerHTML = `<p>${this._header}</p>`;
        this._args.forEach(formElement => {
        this.form.appendChild(formElement.render());
        });

        this.form.addEventListener('submit', this.functionSubmit);

        this.select = document.getElementById('select-doctor');
        console.log(this.select);

        if(this.select) {
            this.select._select.addEventListener('change', selectChangeHandler);
        }
        return this.form;
    }
    // collectingDataJSON() {
    //     const objJson = {};
    //     const inputs = this.form.querySelectorAll('input');
    //     inputs.forEach(input => {
    //         objJson[input.name] = input.value
    //     });
    //     console.log(objJson);
    //     return objJson;
    // }
};

function selectChangeHandler() {
    const selectedText = this.select[this._select.selectedIndex].text;
    console.log(selectedText);
    switch (selectedText) {
            case 'Дантист':
                const formDantist = document.createElement('form');
                formDantist.childNodes.innerHTML = '';
                formDantist.innerHTML = `<div class='doctors-form'>
                                            <p>Цель визита: <input type='text'></input></p>
                                            <p>Краткое описание визит: <textarea rows="3" cols="45"></textarea></p>
                                            <selector></selector>
                                            <input type='submit'></input>
                                        </div>`;
                const selectModalWindow = document.querySelector('.visit-modal-content');
                console.log(selectModalWindow);
                selectModalWindow.append(formDantist);


                break;
            case 'Кардиолог':
                const formCardiologist = document.createElement('form');
                formCardiologist.innerHTML = `
                    <div class='doctors-form'>
                        <input type='text' placeholder='enter your doctor's name'></input>
                        <input type='submit'></input>
                        </div>
                `;
                const selectModalWindoww = document.querySelector('.visit-modal-content');
                console.log(selectModalWindoww);

                selectModalWindoww.append(formCardiologist);
                break;
            case 'Терапевт':
                // chooseTherapist();
                break;
            default: ''
        }
}

async function functionSubmit(e) {
    const btnSubmit = document.getElementById("submit");
    e.preventDefault();

    const objJson = {};
    const input = document.querySelectorAll('input');
    for (let item of input) {
        if (item.type !== 'submit') {
            objJson[item.name] = item.value;
        }
    };
    console.log("objJson", objJson);

    let response = await fetch('http://cards.danit.com.ua/login', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify(objJson)
    });

    let result = await response.json();

    localStorage.setItem("token", result.token);

    console.log(result);

    if (result.status !== "Error") {
        const openAutoForm = document.querySelector('#btn');
        openAutoForm.remove();
        const modalWindow = document.querySelector('.modal-content');
        modalWindow.remove();


        const button = document.getElementById("create-visit-modal");
        button.classList.add('active');

    } else {
        // alert("Enter correct login or password");
        const error = document.querySelector('.error-message') ? document.querySelector('.test') : document.createElement('p');
        error.classList.add('error-message')
        error.innerText = 'Enter correct login or password';
        const formError = document.querySelector('form');
        document.querySelector('input').focus();
        document.getElementById('qwe').value = '';
        document.getElementById('asd').value = '';

        formError.appendChild(error);
    }
};



class Input {
    constructor(id, name, type, placeholder, ...classList) {
        // this._value = value,
        this._id = id,
            this._name = name,
            this._type = type,
            this._placeholder = placeholder || '',
            this._classList = classList.join(' '),
            this._inputElement = null
    }
    render() {
        this._inputElement = document.createElement('input');
        // inputElement.value = this._value;
        this._inputElement.id = this._id,
            this._inputElement.name = this._name;
        this._inputElement.type = this._type;
        this._inputElement.placeholder = this._placeholder;
        this._inputElement.classList = this._classList;
        return this._inputElement;
    }
}


const inputLogin = new Input('qwe', 'email', 'text', 'введите ваш логин', ['input-login, input-autorization']);
const inputPassword = new Input('asd', 'password', 'text', 'введите ваш пароль', ['input-password, input-autorization']);
const inputSubmit = new Input('submit', 'submit-button', 'submit', 'submit-btn', ['button-submit']);
const form = new Form('Вход в систему', ['form-registration'], functionSubmit, inputLogin, inputPassword, inputSubmit);
const modal = new Modal(['modal'], form);

modal.render(document.body);

const inputDoctorName = new Input('doctor-name', 'doctor-name', 'введите имя доктора', ['input-doctor-name']);
const inputPurposeOfVisit = new Input('doctor-purpose', 'doctor-purpose', 'введите цель визита', ['input-doctor-purpose']);



class VisitModal {
    constructor(classList, ...elements) {
        this._classList = classList.join(' '),
            this._elements = elements,
            this._visitModal = null,
            this.select = null
    }
    render(container) {
        this._visitModal = document.createElement('div');
        this._visitModal.className = 'visit-modal-content';
        this._visitModal.innerHTML = `
        <span class='close'>x</span>
 `;
        this._elements.forEach(element => {
            console.log(element);
            this._visitModal.appendChild(element.render());
        });
        const createVisitBtn = document.getElementById('create-visit-modal');
        createVisitBtn.addEventListener('click', () => {
            selectModal.show();
            

            document.body.append(this._visitModal);
            const selectingDoctor = document.getElementById('select-doctor');
            selectingDoctor.addEventListener('change', () => {
                console.log('hi');
                const selectedText = this.select[this._select.selectedIndex].text;
                console.log(this.select);
            })
        });

        this._visitModal.addEventListener('click', (event) => {
            if (event.target.classList.contains('close')) {
                this.hide();
            };
        });
    }

    show() {
        this._visitModal.classList.add('active');
    }
    hide() {
        this._visitModal.classList.remove('active');
    }
};

class Select {
    constructor(size, name, id, ...args) {
        this._size = size,
            this._name = name,
            this._id = id,
            this._args = args,
            this._select = null
    }
    render() {
        this._select = document.createElement('select');
        this._select.size = this._size,
            this._select.name = this._name,
            this._select.id = this._id,
            this._args.map(item => this._select.append(item));
        // this.selectedDoctor = this.selectDoctor.bind(this),
        // this._select.addEventListener('change', this.selectedDoctor);

        return this._select
    }
};
function submitHandler(e) {
    e.preventDefault();
    const visitData = {};
    const inputVisit = document.querySelectorAll('input');
    for (let item of inputVisit) {
        if (item.type !== 'submit') {
            visitData[item.name] = item.value;
        }
    };
}
class Option {
    constructor(name, value) {
        this._name = name,
            this._value = value,
            this.option = null
    }
    render() {
        this.option = document.createElement('option');
        this.option.name = this._name;
        this.option.innerHTML = `<option>${this._name}</option>`;
        this.option.value = this._value;
        return this.option
    }
};


const option1 = new Option('Выберите врача', '', selected = 'selected').render();
const option2 = new Option('Дантист', 'VisitDentist').render();
const option3 = new Option('Кардиолог', 'VisitCardiologist').render();
const option4 = new Option('Терапевт', 'VisitTherapist').render();

// const inputSelectSubmit = new Input('submitSelect', 'submit-button', 'submit', '', ['button-select-submit']);

const selectForm = new Select('1', 'select doctor', 'select-doctor', option1, option2, option3, option4);
console.log(selectForm);
const formSelect = new Form('Выберите интерисующего Вас врача', ['form-select'], selectChangeHandler, selectForm);
// document.body.append(formSelect.render());

const selectModal = new VisitModal(['select-modal-window'], formSelect);
console.log(selectModal);
selectModal.render(document.body);


/**************************************************/

class Visit {
    constructor(patientName, doctorType, visitDate, id) {
        this._patientName = patientName,
            this._doctorType = doctorType,
            this._visitDate = visitDate,
            this._id = id,
            this.block = null;
    }
    render() {
        this.block = document.createElement("div");
        this.innerHTML = `
            <p>${this._patient}</p>  
            <p>${this._doctorType}</>
            <p>${this._visitDate}</>
        `;
    }
}

class CardioVisit extends Visit {
    constructor(presure, weightIndex, ...args) {
        super(...args);
        this._presure = presure;
    }

    render() {
        super.render();
        this.block.insertAdjacentHTML("beforeend", `<p>${this.pressure}</p>`);
    }
}
const visits = [];
// axios.post(url, visitData).then(({data}) => {
//    const cardio = new CardioVisit(data.presure, data.weightIndex, data.patient, data.doctor, data.id);
//    const block = cardio.render();
//    container.append(block);
//    visits.push(cardio);
// });













